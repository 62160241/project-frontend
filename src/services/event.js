/* eslint-disable space-before-function-paren */
import api from './api'

export function getReserves(startDate, endDate, roomSelect) {
  return api.get('/getReserveByRoom/rooms/' + roomSelect, { params: { startDate: startDate, endDate: endDate } })
}
