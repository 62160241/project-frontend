import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/room',
    name: 'Room',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Room.vue')
  },
  {
    path: '/reservation',
    name: 'Reservation',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Reservation.vue')
  },
  {
    path: '/reserve_status',
    name: 'Reserve_Status',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/reversationstatus/ReserveStatus.vue')
  },
  {
    path: '/history',
    name: 'History',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/History.vue')
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/consider_booking',
    name: 'Consider_Booking',
    component: () => import(/* webpackChunkName: "about" */ '../views/consider/ConsiderTable.vue')
  },
  {
    path: '/user_management',
    name: 'User_Management',
    component: () => import(/* webpackChunkName: "about" */ '../views/manageUser/ManageUserTable.vue')
  },
  {
    path: '/room_management',
    name: 'Room_Management',
    component: () => import(/* webpackChunkName: "about" */ '../views/room/RoomTable.vue')
  },
  {
    path: '/building_management',
    name: 'Building_Management',
    component: () => import(/* webpackChunkName: "about" */ '../views/building/BuildingTable.vue')
  },
  {
    path: '/agency_management',
    name: 'Agency_Management',
    component: () => import(/* webpackChunkName: "about" */ '../views/faculty/FacultyTable.vue')
  },
  {
    path: '/equipment_management',
    name: 'Equipment_Management',
    component: () => import(/* webpackChunkName: "about" */ '../views/equipment/EquipmentTable.vue')
  },
  {
    path: '/approval_sequences_management',
    name: 'Approval_Sequences_Management',
    component: () => import(/* webpackChunkName: "about" */ '../views/order_considerations/order_considerationsTable.vue')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
