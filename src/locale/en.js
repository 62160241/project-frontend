export default {
  weekDays: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
  weekDaysShort: ['Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.', 'Sun.'],
  months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  years: 'Year',
  year: 'This Year',
  month: 'Month',
  week: 'Week',
  day: 'Day',
  today: 'Today',
  noEvent: 'No Event',
  allDay: 'All Day',
  deleteEvent: 'Delete',
  createEvent: 'New Event',
  dateFormat: 'YYYY MMMM D dddd'
}
